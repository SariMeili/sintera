// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "SinteraGameMode.h"
#include "SinteraCharacter.h"
#include "UObject/ConstructorHelpers.h"

ASinteraGameMode::ASinteraGameMode()
{
	// set default pawn class to our Blueprinted character
	//static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/UE4_Template/ThirdPersonCPP/Blueprints/ThirdPersonCharacter.ThirdPersonCharacter_C"));
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Programmer/Blueprints/Character/RacoonCharacter.RacoonCharacter_C"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
