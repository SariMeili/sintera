// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "SinteraCharacter.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"

#include "Engine.h"

//////////////////////////////////////////////////////////////////////////
// ASinteraCharacter

ASinteraCharacter::ASinteraCharacter()
{
#pragma region UnrealInstances

	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 350.f; // corrected 16.03.2020
	GetCharacterMovement()->AirControl = 0.2f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)

#pragma endregion

	// set Sprint values for inputs
	isSprinting = false;
	NormalWalk = GetCharacterMovement()->MaxWalkSpeed;
	SprintWalk = 1200.0f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// HUD Instances
	playerHealth = 1.00f;
	playerStamina = 1.0f;
	playerAddiction = MinAddiction;
	herb1 = 0;
	herb2 = 0;
	herb3 = 0;
	herb4 = 0;

#pragma region AnimMontage & BoxCollider

	// load Animation Montage
	static ConstructorHelpers::FObjectFinder<UAnimMontage> MeleeAttackMontageObject(TEXT("AnimMontage'/Game/Programmer/Blueprints/Character/combatSystem/RacoonAttackMontage.RacoonAttackMontage'"));
	if (MeleeAttackMontageObject.Succeeded())
	{
		RacoonAttackMontage = MeleeAttackMontageObject.Object;
	}

	SetupColliders();

#pragma endregion

}

void ASinteraCharacter::SetupColliders()
{
	// Bind CollisionBoxes on Fists
	LeftFistCollisionBox = CreateDefaultSubobject<UBoxComponent>(TEXT("LeftFistCollisionBox"));
	LeftFistCollisionBox->SetupAttachment(RootComponent);
	LeftFistCollisionBox->SetCollisionProfileName("NoCollision");
	LeftFistCollisionBox->SetNotifyRigidBodyCollision(false);

	RightFistCollisionBox = CreateDefaultSubobject<UBoxComponent>(TEXT("RightFistCollisionBox"));
	RightFistCollisionBox->SetupAttachment(RootComponent);
	RightFistCollisionBox->SetCollisionProfileName("NoCollision");
	RightFistCollisionBox->SetNotifyRigidBodyCollision(false);
}

//////////////////////////////////////////////////////////////////////////
// Input

void ASinteraCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAxis("MoveForward", this, &ASinteraCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ASinteraCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &ASinteraCharacter::TurnAtRate);
	//PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUp", this, &ASinteraCharacter::LookUpAtRate);
	PlayerInputComponent->BindAxis("LookUpRate", this, &ASinteraCharacter::LookUpAtRate);

	// handle touch devices
	PlayerInputComponent->BindTouch(IE_Pressed, this, &ASinteraCharacter::TouchStarted);
	PlayerInputComponent->BindTouch(IE_Released, this, &ASinteraCharacter::TouchStopped);

	// VR headset functionality
	PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &ASinteraCharacter::OnResetVR);

	PlayerInputComponent->BindAction("Sprint", IE_Pressed, this, &ASinteraCharacter::Sprint);
	PlayerInputComponent->BindAction("Sprint", IE_Released, this, &ASinteraCharacter::StopSprinting);

	PlayerInputComponent->BindAction("Attack", IE_Pressed, this, &ASinteraCharacter::AttackInput);
}

#pragma region UnrealMovement

void ASinteraCharacter::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void ASinteraCharacter::TouchStarted(ETouchIndex::Type FingerIndex, FVector Location)
{
	Jump();
}

void ASinteraCharacter::TouchStopped(ETouchIndex::Type FingerIndex, FVector Location)
{
	StopJumping();
}

void ASinteraCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void ASinteraCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void ASinteraCharacter::MoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void ASinteraCharacter::MoveRight(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}

#pragma endregion

void ASinteraCharacter::BeginPlay()
{
	Super::BeginPlay();

	// attach collision components to sockets
	const FAttachmentTransformRules AttachmentRules(EAttachmentRule::SnapToTarget, EAttachmentRule::SnapToTarget, EAttachmentRule::KeepWorld, false);

	LeftFistCollisionBox->AttachToComponent(GetMesh(), AttachmentRules, "fist_l_collision");
	RightFistCollisionBox->AttachToComponent(GetMesh(), AttachmentRules, "fist_r_collision");

	GetMesh()->SetMaterial(0, NormalMaterial);
}

void ASinteraCharacter::Sprint()
{
	if (!isSprinting && !isExhausted && playerStamina > 0)
	{
		isSprinting = true;
		GetCharacterMovement()->MaxWalkSpeed = SprintWalk;
	}
}

void ASinteraCharacter::StopSprinting()
{
	isSprinting = false;
	GetCharacterMovement()->MaxWalkSpeed = NormalWalk;
}

void ASinteraCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	float staminaLose = 0.5f;

	if (isSprinting)
	{
		if (playerStamina == 0 && !isExhausted)
		{
			isSprinting = false;
			isExhausted = true;
			GetCharacterMovement()->MaxWalkSpeed = NormalWalk;
		}
		else
		{
			playerStamina -= (DeltaSeconds * staminaLose);
			playerStamina = FMath::Clamp<float>(playerStamina, 0, MaxStamina);
			GetCharacterMovement()->MaxWalkSpeed = SprintWalk;
		}
	}
	else
	{
		// recover Stamina
		if (playerStamina < MaxStamina)
		{
			playerStamina += DeltaSeconds;
			playerStamina = FMath::Clamp<float>(playerStamina, 0, MaxStamina);
		}

		if (playerStamina >= MaxStamina / 2.0f)
		{
			isExhausted = false;
		}
	}
}

#pragma region HUD_Functions

void ASinteraCharacter::Heal(float healAmount)
{
	playerHealth += healAmount;

	//playerHealth = FMath::Clamp<float>(playerHealth, 0, 1);
	if (playerHealth > 1.0f)
	{
		playerHealth = 1.0f;
	}
}

void ASinteraCharacter::AddAddiction(float addictionAmount)
{
	isAffected = true;
	playerAddiction += addictionAmount;

	//playerAddiction = FMath::Clamp<float>(playerAddiction, MinAddiction, 1);
	if (playerAddiction > 1.0f)
	{
		playerAddiction = 1.0f;
	}
}

void ASinteraCharacter::LoseAddiction(float addictionAmount)
{
	isAffected = false;
	playerAddiction -= addictionAmount;

	//playerAddiction = FMath::Clamp<float>(playerAddiction, MinAddiction, 1);
	if (playerAddiction < MinAddiction)
	{
		playerAddiction = MinAddiction;
	}
}

void ASinteraCharacter::AddToInventoryText(EHerbType HerbType)
{
	switch (HerbType)
	{
	case EHerbType::HT_Herb1:
		herb1++;
		break;
	case EHerbType::HT_Herb2:
		herb2++;
		break;
	case EHerbType::HT_Herb3:
		herb3++;
		break;
	case EHerbType::HT_Herb4:
		herb4++;
		break;
	default:
		break;
	}
}

void ASinteraCharacter::ConsumeHerb(EHerbType HerbType, float healthAmount, float addictionAmount, float StaminaAmount)
{
	if (HerbType == EHerbType::HT_Herb1 && herb1 > 0)
	{
		herb1--;
		Heal(healthAmount);
		LoseAddiction(addictionAmount);
		MinAddiction -= addictionAmount;
		playerStamina += StaminaAmount;
	}

	if (HerbType == EHerbType::HT_Herb2 && herb2 > 0)
	{
		herb2--;
		TakeDamage(healthAmount);
		LoseAddiction(addictionAmount);
		MinAddiction -= addictionAmount;
		playerStamina += StaminaAmount;
	}

	if (HerbType == EHerbType::HT_Herb3 && herb3 > 0)
	{
		herb3--;
		TakeDamage(healthAmount);
		AddAddiction(addictionAmount);
		MinAddiction -= addictionAmount;
		playerStamina += StaminaAmount;
	}

	if (HerbType == EHerbType::HT_Herb4 && herb4 > 0)
	{
		herb4--;
		Heal(healthAmount);
		AddAddiction(addictionAmount);
		MinAddiction += addictionAmount;
		playerStamina -= StaminaAmount;
	}
}

#pragma endregion


#pragma region Attack_Functions

void ASinteraCharacter::AttackInput()
{
	/*
	// generate random number (1-2)
	int MontageSectionIndex = FMath::RandRange(1, 2);

	// fstring animation section
	FString MontageSection = "Start_" + FString::FromInt(MontageSectionIndex);

	PlayAnimMontage(RacoonAttackMontage, 1.f, FName(*MontageSection));
	*/

	if (!GetMesh()->GetAnimInstance()->Montage_IsPlaying(RacoonAttackMontage))
	{
		PlayAnimMontage(RacoonAttackMontage, 1.f, FName("Start_1"));
	}
}

void ASinteraCharacter::StartAttack()
{
	LeftFistCollisionBox->SetActive(true);
	LeftFistCollisionBox->SetCollisionProfileName("Weapon");
	LeftFistCollisionBox->SetNotifyRigidBodyCollision(true);

	RightFistCollisionBox->SetActive(true);
	RightFistCollisionBox->SetCollisionProfileName("Weapon");
	RightFistCollisionBox->SetNotifyRigidBodyCollision(true);
}

void ASinteraCharacter::StopAttack()
{
	LeftFistCollisionBox->SetCollisionProfileName("NoCollision");
	LeftFistCollisionBox->SetNotifyRigidBodyCollision(false);
	LeftFistCollisionBox->SetActive(false);

	RightFistCollisionBox->SetCollisionProfileName("NoCollision");
	RightFistCollisionBox->SetNotifyRigidBodyCollision(false);
	RightFistCollisionBox->SetActive(false);
}

void ASinteraCharacter::TakeDamage(float damageAmount)
{
	playerHealth -= damageAmount;

	playerHealth = FMath::Clamp<float>(playerHealth, 0, 1);

	GetWorld()->GetTimerManager().SetTimer(_HitHandle, this, &ASinteraCharacter::OnTimerEnd, .15f, false);
}

void ASinteraCharacter::OnTimerEnd()
{
	GetMesh()->SetMaterial(0, NormalMaterial);
}

#pragma endregion
