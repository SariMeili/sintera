// Fill out your copyright notice in the Description page of Project Settings.


#include "AI_Character.h"
#include "Sintera.h"
#include "SinteraCharacter.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "AI_Controller.h"

#include "Components/BoxComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Engine.h"


// Sets default values
AAI_Character::AAI_Character()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	GetCharacterMovement()->bOrientRotationToMovement = true;
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 600.0f, 0.0f);

	aiHealth = 1.00f;

#pragma region Setup AnimMontage & BoxCollider

	// load Animation Montage
	static ConstructorHelpers::FObjectFinder<UAnimMontage> MeleeAttackMontageObject(TEXT("AnimMontage'/Game/Programmer/Blueprints/Character/combatSystem/LynxAttackMontage.LynxAttackMontage'"));
	if (MeleeAttackMontageObject.Succeeded())
	{
		AIAttackMontage = MeleeAttackMontageObject.Object;
	}

	SetupColliders();

#pragma endregion
}

void AAI_Character::SetupColliders()
{
	SpearCollisionBox = CreateDefaultSubobject<UBoxComponent>(TEXT("LeftFistCollisionBox"));
	SpearCollisionBox->SetupAttachment(RootComponent);
	SpearCollisionBox->SetCollisionProfileName("NoCollision");
	SpearCollisionBox->SetNotifyRigidBodyCollision(false);

	HitBox = CreateDefaultSubobject<UBoxComponent>(TEXT("HitBox"));
	HitBox->SetupAttachment(RootComponent);
	HitBox->SetNotifyRigidBodyCollision(true);
}

// Called when the game starts or when spawned
void AAI_Character::BeginPlay()
{
	Super::BeginPlay();

	// attach collision components to sockets
	const FAttachmentTransformRules AttachmentRules(EAttachmentRule::SnapToTarget, EAttachmentRule::SnapToTarget, EAttachmentRule::KeepWorld, false);

	SpearCollisionBox->AttachToComponent(GetMesh(), AttachmentRules, "spearCollider");

	HitBox->AttachToComponent(GetMesh(), AttachmentRules, "hitbox");

	HitBox->OnComponentBeginOverlap.AddDynamic(this, &AAI_Character::OnOverlapBegin);
	HitBox->OnComponentEndOverlap.AddDynamic(this, &AAI_Character::OnOverlapEnd);

	GetMesh()->SetMaterial(0, NormalMaterial);
}

// Called every frame
void AAI_Character::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (aiHealth <= 0.0f)
	{
		Destroy();
	}
}

// Called to bind functionality to input
void AAI_Character::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

void AAI_Character::AITakeDamage(float damageAmount)
{
	aiHealth -= damageAmount;

	aiHealth = FMath::Clamp<float>(aiHealth, 0, 1);

	GetWorld()->GetTimerManager().SetTimer(_HitHandle, this, &AAI_Character::OnTimerEnd, .15f, false);
}

#pragma region Attack_Functions

void AAI_Character::AIAttackInput()
{
	/*
	// generate random number (1-2)
	int MontageSectionIndex = FMath::RandRange(1, 2);

	// fstring animation section
	FString MontageSection = "Start_" + FString::FromInt(MontageSectionIndex);

	PlayAnimMontage(AIAttackMontage, 1.f, FName(*MontageSection));
	*/

	if (!GetMesh()->GetAnimInstance()->Montage_IsPlaying(AIAttackMontage))
	{
		PlayAnimMontage(AIAttackMontage, 1.f, FName("Start_1"));
	}
}

void AAI_Character::AIStartAttack()
{
	SpearCollisionBox->SetCollisionProfileName("Weapon");
	SpearCollisionBox->SetActive(true);
	SpearCollisionBox->SetNotifyRigidBodyCollision(true);
}

void AAI_Character::AIStopAttack()
{
	SpearCollisionBox->SetCollisionProfileName("NoCollision");
	SpearCollisionBox->SetActive(false);
	SpearCollisionBox->SetNotifyRigidBodyCollision(false);
}

void AAI_Character::OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	//ASinteraCharacter* Player = Cast<ASinteraCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
	if ((OtherActor != this) && OtherActor->ActorHasTag(TEXT("Player")) && OtherComp)
	{
		AIAttackInput();
	}
}

void AAI_Character::OnOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (OtherActor && (OtherActor != this) && OtherComp)
	{
		AIStopAttack();
	}
}

void AAI_Character::OnTimerEnd()
{
	GetMesh()->SetMaterial(0, NormalMaterial);
}

#pragma endregion
