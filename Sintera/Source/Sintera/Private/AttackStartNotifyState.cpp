// Fill out your copyright notice in the Description page of Project Settings.


#include "AttackStartNotifyState.h"
#include "Sintera.h"
#include "SinteraCharacter.h"
#include "AI_Character.h"
#include "Engine.h"

void UAttackStartNotifyState::NotifyBegin(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, float TotalDuration)
{
	if (MeshComp != NULL && MeshComp->GetOwner() != NULL)
	{
		ASinteraCharacter* Player = Cast<ASinteraCharacter>(MeshComp->GetOwner());
		AAI_Character* AICharacter = Cast<AAI_Character>(MeshComp->GetOwner());

		if (Player != NULL && MeshComp->GetOwner() == Player)
		{
			Player->StartAttack();
		}
		// used if Player and AI uses same Script to approch Animation
		else if (AICharacter != NULL && MeshComp->GetOwner() == AICharacter)
		{
			AICharacter->AIStartAttack();
		}
	}
}

void UAttackStartNotifyState::NotifyEnd(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation)
{
	if (MeshComp != NULL && MeshComp->GetOwner() != NULL)
	{
		ASinteraCharacter* Player = Cast<ASinteraCharacter>(MeshComp->GetOwner());
		AAI_Character* AICharacter = Cast<AAI_Character>(MeshComp->GetOwner());

		if (Player != NULL && MeshComp->GetOwner() == Player)
		{
			Player->StopAttack();
		}
		// used if Player and AI uses same Script to approch Animation
		else if (AICharacter != NULL && MeshComp->GetOwner() == AICharacter)
		{
			AICharacter->AIStopAttack();
		}
	}
}
