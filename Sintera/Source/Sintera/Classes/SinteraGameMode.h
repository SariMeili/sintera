// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SinteraCharacter.h"
#include "SinteraGameMode.generated.h"

UCLASS(minimalapi)
class ASinteraGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ASinteraGameMode();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Respawn")
		ASinteraCharacter* player;
};



