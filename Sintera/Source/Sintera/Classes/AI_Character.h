// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Components/BoxComponent.h"
#include "Engine.h"
#include "AI_Character.generated.h"

UCLASS()
class SINTERA_API AAI_Character : public ACharacter
{
	GENERATED_BODY()

/*------------------- Definition: AnimMontage & BoxCollider ---------------------------------------------------*/

	// Melee fist Montage
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Animation, meta = (AllowPrivateAccess = "true"))
		class UAnimMontage* AIAttackMontage;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Collision, meta = (AllowPrivateAccess = "true"))
		class UBoxComponent* SpearCollisionBox;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Collision, meta = (AllowPrivateAccess = "true"))
		class UBoxComponent* HitBox;

/*-------------------------------------------------------------------------------------------------------------*/


public:
	// Sets default values for this character's properties
	AAI_Character();

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

/*-------------------------------------------------------------------------------------------------------------*/

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI_Material")
		UMaterialInterface* NormalMaterial;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class AWaypoint* NextWaypoint;

	// AI health
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI_Health")
		float aiHealth;

	// Create & Bind CollisionBoxes
	UFUNCTION()
		void SetupColliders();

	// take Damage Setup
	UFUNCTION(BlueprintCallable, Category = "AI_Health")
		void AITakeDamage(float damageAmount);

	// Triggers attack animation
	UFUNCTION(BlueprintCallable, Category = "AI_Attack")
		void AIAttackInput();

	// attack-Method
	void AIStartAttack();
	void AIStopAttack();

	//overlap event (hitbox)
	UFUNCTION()
		void OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
		void OnOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION()
		void OnTimerEnd();

protected:

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:

	FTimerHandle _HitHandle;

};
