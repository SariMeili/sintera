// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Components/BoxComponent.h"
#include "SinteraCharacter.generated.h"

UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class EHerbType : uint8
{
	HT_Herb1 	UMETA(DisplayName = "Herb1"),
	HT_Herb2 	UMETA(DisplayName = "Herb2"),
	HT_Herb3	UMETA(DisplayName = "Herb3"),
	HT_Herb4	UMETA(DisplayName = "Herb4")
};

UCLASS(config = Game)
class ASinteraCharacter : public ACharacter
{
	GENERATED_BODY()

		/** Camera boom positioning the camera behind the character */
		UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UCameraComponent* FollowCamera;

	/*------------------- Definition: AnimMontage & BoxCollider ---------------------------------------------------*/

			// Melee fist Montage
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Animation, meta = (AllowPrivateAccess = "true"))
		class UAnimMontage* RacoonAttackMontage;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Collision, meta = (AllowPrivateAccess = "true"))
		class UBoxComponent* LeftFistCollisionBox;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Collision, meta = (AllowPrivateAccess = "true"))
		class UBoxComponent* RightFistCollisionBox;

	/*-------------------------------------------------------------------------------------------------------------*/

public:
	ASinteraCharacter();

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		float BaseLookUpRate;


	// called, when game starts or player spawns
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Create & Bind CollisionBoxes
	UFUNCTION()
		void SetupColliders();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Material")
		UMaterialInterface* NormalMaterial;

	UFUNCTION()
		void OnTimerEnd();

	/*-------------------- definition: HUD/UI & Attack Setups -------------------------------------------------------------------------------------------------*/

			// setup for Sprint
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Sprint")
		float NormalWalk;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Sprint")
		float SprintWalk;

	// player health
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
		float playerHealth;

	// addiction level of player
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Addiction")
		float playerAddiction;

	// Max Stamina player has
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Addiction")
		float MinAddiction = 0.3f;

	//ccurrent endurance/stamina
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stamina")
		float playerStamina;

	// Max Stamina player has
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stamina")
		float MaxStamina = 1.0f;

	// Amount of collectabales
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		int herb1;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		int herb2;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		int herb3;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		int herb4;

	// add to inventory fuction
	UFUNCTION(BlueprintCallable, Category = "Inventory")
		void AddToInventoryText(EHerbType HerbType);

	UFUNCTION(BlueprintCallable, Category = "Inventory")
		void ConsumeHerb(EHerbType HerbType, float healthAmount, float addictionAmount, float StaminaAmount);

	// take Damage Setup
	UFUNCTION(BlueprintCallable, Category = "Health")
		void TakeDamage(float damageAmount);

	// Triggers attack animation
	void AttackInput();

	// attack-Method
	void StartAttack();
	void StopAttack();
	/*---------------------------------------------------------------------------------------------------------------------------------------------------------*/

protected:

	/** Resets HMD orientation in VR. */
	void OnResetVR();

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	/**
	 * Called via input to turn at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	/** Handler for when a touch input begins. */
	void TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);

	/** Handler for when a touch input stops. */
	void TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);

	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface

	// Allows player to begin sprint
	void Sprint();

	// Allows player to stop sprint
	void StopSprinting();

	/*----------------------- definition: UI bools & functions -----------------------------------------------*/

	// determines when player is sprinting
	bool isSprinting;

	// Indicates that the player is exhausted
	bool isExhausted = false;

	// determines if player is affected by drugs
	bool isAffected = false;

	UFUNCTION(BlueprintCallable, Category = "Health")
		void Heal(float healAmount);

	// update addiction Setup
	UFUNCTION(BlueprintCallable, Category = "Addiction")
		void AddAddiction(float addictionAmount);

	UFUNCTION(BlueprintCallable, Category = "Addiction")
		void LoseAddiction(float addictionAmount);

private:

	FTimerHandle _HitHandle;

	/*--------------------------------------------------------------------------------------------------------*/

public:
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }
};

